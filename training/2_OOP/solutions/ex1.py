import math

class Point():
    def __init__(self,x,y):
        self.x = x
        self.y = y

    def distance(self,other):
        a=(self.x-other.x)**2
        b=(self.y-other.y)**2
        return math.sqrt(a+b)

    def move(self,X,Y):
        self.x+=X
        self.y+=Y

    def __repr__(self):
        return(f"P=({self.x},{self.y})")



