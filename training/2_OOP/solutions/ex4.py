import json


class ContactManager(): 
    def __init__(self, fileName):
        self.fileName = fileName
        self.contactsList = []

    def loadContacts(self):
        fp = open(self.fileName)
        td = json.load(fp)
        print(td)
        for contact in td["contacts"]:
            newContact={
                "name": contact["name"],
                 "mail": contact["mail"],
                  "surname":contact["surname"]
                  }
            self.contactsList.append(newContact)

    def showContacts(self):
        for contact in self.contactsList:
            print(contact)
    
if __name__=="__main__":
    cm=ContactManager("../usefulFiles/contacts.json")
    cm.loadContacts()
    cm.showContacts()
    #json.dump(contactsList,open("contacts.json",'w'))
