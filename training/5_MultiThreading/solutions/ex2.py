import numpy as np
import threading
import time
import json

N_CUSTOMERS=30
N_DESKS=5
CASH_DESKS=[i for i in range(N_DESKS) ]
CUSTOMERS=[np.random.poisson(10) for i in range(N_CUSTOMERS)]

SERVED=dict([[i,0] for i in range(len(CASH_DESKS))])
BUSY=dict([[i,0] for i in range(len(CASH_DESKS))])



class Customer(threading.Thread):
    def __init__(self,customerID,servingTime,semaphore):
        threading.Thread.__init__(self,name=customerID)
        self.servingTime=servingTime
        self.customerID=customerID
        self.semaphore=semaphore

    def run(self):
        self.semaphore.acquire()
        servedBy=CASH_DESKS.pop(0)
        SERVED[servedBy]+=1
        BUSY[servedBy]+=self.servingTime
        CASH_DESKS.append(servedBy)
        self.semaphore.release()


if __name__=="__main__":
    customers=[]
    T_semaphore=threading.Semaphore(len(CASH_DESKS))

    for i in range(N_CUSTOMERS):
        customers.append(Customer(i,CUSTOMERS[i],T_semaphore))

    start=time.time()
    for x in customers:
        x.start()
        x.join()
    end=time.time()

    #Calculate stats
    TOTAL_SERVING_TIME=end-start
    AVG_PER_DESK=[BUSY[i]/SERVED[i] for i in range(N_DESKS)]
    print(f"STATS:Total serving time: {TOTAL_SERVING_TIME}")
    print (f"Customer per desk:{json.dumps(SERVED,indent=4)}")
    print (f"Average time per desk:{AVG_PER_DESK}")

        
