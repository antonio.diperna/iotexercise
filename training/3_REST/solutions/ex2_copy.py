import cherrypy
import json
class ParamsLister():
    exposed=True

    def __init__(self):
        pass
    
    def PUT(self):
        body=cherrypy.request.body.read()
        jsonBody=json.loads(body)
        return f" Keys :{list(jsonBody.keys())}, values :{list(jsonBody.values())}"

if __name__=="__main__":
    #Standard configuration to serve the url "localhost:8080"
    conf={
        '/':{
                'request.dispatch':cherrypy.dispatch.MethodDispatcher(),
                'tool.session.on':True
        }
    }
    cherrypy.config.update({'server.socket_port':8090})
    cherrypy.quickstart(ParamsLister(),'/',conf)