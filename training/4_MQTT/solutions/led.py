from MyMQTT import *
import json
import time

class Led():
    def __init__(self,clientID,topic,broker, port):
        self.client=MyMQTT(clientID,broker,port,self)
        self.topic=topic
        self.status=None
    
    def start(self):
        self.client.start()
        self.client.mySubscribe(self.topic)

    def stop(self):
        self.client.stop()

    def notify(self,topic,msg):
        payload=json.loads(msg)
        new_status=payload["value"]
        self.status=new_status
        pubID=payload["clientID"]
        timestamp=payload["timestamp"]
        print(f"The led has been set to {new_status} at {timestamp} by {pubID}")


if __name__=="__main__":
    conf=json.load(open("settings.json"))
    broker=conf["broker"]
    port=conf["port"]
    myLed=Led("myLed178398434","IoT/Orlando/led",broker,port)
    myLed.start()
    while True:
        time.sleep(3)
    myLed.stop()