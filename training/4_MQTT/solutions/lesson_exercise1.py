from MyMQTT import *
import json
import time

class LedManager():
    def __init__(self,clientID,topic,broker, port):
        self.client=MyMQTT(clientID,broker,port,None)
        self.topic=topic
        self.__message={"clientID":clientID,"n":"switch","value":None,"timestamp":'',"unit":"bool"}
    
    def start(self):
        self.client.start()

    def stop(self):
        self.client.stop()

    def publish(self,value):
        message=self.__message
        message['value']=value
        message["timestamp"]=str(time.time())
        self.client.myPublish(self.topic,message)

if __name__=="__main__":
    conf=json.load(open("settings.json"))
    broker=conf["broker"]
    port=conf["port"]
    ledManager=LedManager('ledManager762389234',"IoT/Orlando/led",broker,port)
    ledManager.start()
    for i in range(10):
        if i%2==0:
            ledManager.publish("on")
            print("pippo")
        else:
            ledManager.publish("off")
        time.sleep(3)
    ledManager.stop()